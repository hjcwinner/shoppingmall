const mongoose = require('mongoose')
const orderSchema = mongoose.Schema({
    product : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'product',
        required : true
    },
    quantity : {
        type : Number,
        required : true
    }
})

module.exports = mongoose.model('oder', orderSchema)
