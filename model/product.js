const mongoose = require('mongoose');

const mongooseSchema = mongoose.Schema({
    name : {
        type : String,     ////, 조심조심
        required : true    
    },
    price : {
        type :Number,
        required : true    
    }
});

module.exports = mongoose.model('product', mongooseSchema);
