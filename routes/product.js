const express = require('express');
const router = express.Router();
const productModel = require('../model/product');  ////스키마(테이블)쓰겠다고 선언
const product = require('../model/product');

/////불러오기
router.get("/", (req, res) => {
    productModel
        .find()
        .then(docs => {
            res.json({
                count : docs.length,
                products : docs.map(doc => {
                    return{
                        id : doc._id,
                        name : doc.name,
                        price : doc.price,
                        request : {
                            type : "get",
                            url : "http://localhost:7070/product/" + doc._id
                        }
                    }
                })
            })    
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
});

//////한개불러오기
router.get("/:productid", (req, res) => {
    productModel
        .findById(req.params.productid)
        .then(doc => {
            if(!doc)
            {
                res.json({
                    message : "그런상품없음"
                })
            }
            else
            {
                res.json({
                    message : "product one open  " +  req.params.productid,
                    productInfo : {
                        id : doc._id,
                        name : doc.name,
                        price : doc.price,
                        request : {
                            type : "get",
                            url : "http://localhost:7070/product/"
                        }
                    }
                })
            }
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
})


/////등록하기
router.post("/", (req, res) => {
    const product = new productModel({
        name : req.body.productname,         ////제발 , 까먹지마라
        price : req.body.productprice,
    })
    product
        .save()
        .then(doc => {
            res.json({
                message : "prduct save complete",    ////제발 , 까먹지마라
                productIno : {
                    id : doc._id,
                    name : doc.name,
                    price : doc.price,
                    request : {
                        type : "get",
                        url : "http://localhost:7070/product/"
                    }
                }
            })
        })
        .catch(err => {
            res.json({
                error : err.message
            })
        })
});

/////수정하기
router.patch("/:productid", (req, res) => {
    const updateOps = {}
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value
    }

    productModel
        .findByIdAndUpdate(req.params.productid, { $set : updateOps})
        .then(result => {
            res.json({
                message : "update product",
                productInfo : {
                    id : result._id,
                    name : result.name,
                    price : result.price,
                    request : {
                        type : "get",
                        url : "http://localhost:7070/product/" + result._id
                    }
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
});

/////삭제하기
router.delete("/:prodelete", (req, res) => {
    productModel
        .findByIdAndDelete(req.params.prodelete)
        .then(() => {
            res.json({
                message : "delete complete",
                request : {
                    type : "get",
                    url : "http://localhost:7070/product/"
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
});


module.exports = router;
