const express = require('express');
const router = express.Router();
const orderModel = require('../model/order');
const { findById } = require('../model/order');


////불러오기(get)
router.get("/", (req, res) => {
    orderModel
        .find()
        .populate('product',["name","price"])
        .then(aaaa => {
            const response = {
                count : aaaa.length,
                products : aaaa.map(bbb => {
                return{
                    id : bbb._id,
                    product : bbb.product,
                    quantity : bbb.quantity,
                    request : {
                     type : "get",
                     url : "http://localhost:7070/order/" + bbb._id
                    }
                }
            })
        }
        res.json(response)
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
});
////1개불러오기
router.get('/:orderid', (req, res) => {
    orderModel
        .findById(req.params.orderid)
        .populate('product',["name","price"])
        .then(doc => {
            res.json({
                message : "one order get",
                orderInfo : {
                    id : doc._id,
                    product : doc.product,
                    quantity : doc.quantity,
                    request : {
                        type : "get",
                        url : "http://localhost:7070/order"
                    }
                }
            })
        })
})


////등록하기(post)
router.post("/", (req, res) => {
    const order = new orderModel({
        product : req.body.orderid,
        quantity : req.body.qty
    })

    order
        .save()
        .then(doc => { 
            res.json({
                message : "order save",
                orderInfo : {
                    id : doc._id,
                    product : doc.product,
                    quantity : doc.quantity,
                    request : {
                        type : "get",
                        url : "http://localhost:7070/order"
                    }
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })

});

////수정하기(patch)
router.patch("/:orderid", (req, res) => {
    const updateOps = {}
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value
        console.log(updateOps)
    }

    orderModel
        .findByIdAndUpdate(req.params.orderid, { $set : updateOps})
        .then(result => {
            res.json({
                message : "update",
                result : {
                    request : {
                        type : "get",
                        url : "http://localhost:7070/order"
                    }
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
});

////삭제하기(delete)
router.delete("/:orderid", (req, res) => {
    orderModel
        .findByIdAndDelete(req.params.orderid)
        .then(() => {
            res.json({
                message : "order delete",
                result : {
                    type : "get",
                    url : "http://localhost:7070/order"
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
     })
});

module.exports = router;
